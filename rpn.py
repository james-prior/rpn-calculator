#!/usr/bin/env python3

import sys
from collections import deque

MAX_BYTE = 1 << 8


def format_number(x):
    unsigned = x % MAX_BYTE
    signed = unsigned
    if signed >= (MAX_BYTE // 2):
        signed -= MAX_BYTE
    return ' '.join((
        format(unsigned, '08b'),
        format(unsigned, '3d'),
        format(signed, '4d'),
    ))


def format_stack(stack):
    lines = []
    for x in stack:
        lines.append(format_number(x))
    return lines or ['empty']


def parse_to_int(s):
    radix_of_code = {
        'b': 2,
        'o': 0o10,
        'd': 10,
        'x': 0x10,
    }

    try:
        radix_code = s[0]
    except IndexError:
        raise ValueError

    try:
        radix = radix_of_code[radix_code]
    except KeyError:
        radix = 10
    else:
        s = s[1:]

    return int(s, radix)


def parse_line(line, stack):
    binary_functions = {
        '+': lambda x, y: x + y,
        '-': lambda x, y: x - y,
        '&': lambda x, y: x & y,
        '|': lambda x, y: x | y,
        '^': lambda x, y: x ^ y,
        '>>': lambda x, y: x >> y,
        '<<': lambda x, y: x << y,
    }

    line = line.strip()
    if line == 'c':
        stack.clear()
    elif line == '':
        pass
    elif line == 'p':
        if len(stack) < 1:
            return 'ERROR: insufficient stack', stack
        stack.pop()
    elif line == 's':
        if len(stack) < 2:
            return 'ERROR: insufficient stack', stack
        operand2 = stack.pop()
        operand1 = stack.pop()
        stack.append(operand2)
        stack.append(operand1)
    elif line in binary_functions:
        if len(stack) < 2:
            return 'ERROR: insufficient stack', stack
        operand2 = stack.pop()
        operand1 = stack.pop()
        x = binary_functions[line](operand1, operand2)
        stack.append(x)
    elif line == '~':
        if len(stack) < 1:
            return 'ERROR: insufficient stack', stack
        operand = stack.pop()
        x = (~operand) % MAX_BYTE
        stack.append(x)
    else:
        try:
            x = parse_to_int(line)
        except ValueError:
            return 'ERROR: bad input', stack
        stack.append(x)
    return None, stack


def main(lines):
    stack = deque([])
    for line in lines:
        error_message, stack = parse_line(line, stack)
        if error_message:
            print(error_message)
        for line in format_stack(stack):
            print(line)


if __name__ == '__main__':
    main(sys.argv[1:] or sys.stdin)
