from collections import deque

import pytest

from rpn import format_number, parse_to_int, parse_line, format_stack

cases = (
    (0, '00000000   0    0'),
    (1, '00000001   1    1'),
    (2, '00000010   2    2'),
    (127, '01111111 127  127'),
    (128, '10000000 128 -128'),
    (255, '11111111 255   -1'),
    (256, '00000000   0    0'),
    (-1, '11111111 255   -1'),
    (-256, '00000000   0    0'),
)
@pytest.mark.parametrize('number, expected', cases)
def test_format_number(number, expected):
    actual = format_number(number)
    assert expected == actual


cases = (
    ('b0', 0),
    ('b1', 1),
    ('b10', 2),
    ('d10', 10),
    ('d11', 11),
    ('x10', 16),
    ('x100', 256),
    ('o100', 64),
    ('100', 100),
    ('d-10', -10),
)
@pytest.mark.parametrize('input_string, expected', cases)
def test_parse_to_int(input_string, expected):
    actual = parse_to_int(input_string)
    assert expected == actual


cases = (
    'd',
    '.1',
    'b2',
    '',
)
@pytest.mark.parametrize('input_string', cases)
def test_parse_to_int_raises_value_error(input_string):
    with pytest.raises(ValueError):
        parse_to_int(input_string)


stack_expected_outputs = (
    ([], ['empty']),
    ([1], ['00000001   1    1']),
    ([1, -1], ['00000001   1    1', '11111111 255   -1']),
)
@pytest.mark.parametrize(
    'stack, expected_output', stack_expected_outputs)
def test_format_stack(stack, expected_output):
    actual = format_stack(stack)
    assert expected_output == actual


cases = (
    # pushes and formats number
    (('127', deque([])), (None, deque([127]))),
    (('-1', deque([])), (None, deque([-1]))),
    (('x10', deque([])), (None, deque([16]))),
    ((' x10', deque([])), (None, deque([16]))),
    ((' x10', deque([3])), (None, deque([3, 16]))),

    # c clears stack
    (('c', deque([])), (None, deque([]))),
    (('c', deque([3])), (None, deque([]))),

    # '+' adds numbers on stack, pushes and formats sum
    (('+', deque([1, 1])), (None, deque([2]))),

    # '-' subtracts last thing on stack from previous thing on stack,
    #     pushes and formats difference
    (('-', deque([5, 2])), (None, deque([3]))),

    # empty line shows stack
    ((' ', deque([17])), (None, deque([17]))),
    ((' ', deque([5, 2])), (None, deque([5, 2]))),

    # empty line shows 'empty' if nothing on stack
    ((' ', deque([])), (None, deque([]))),

    # bad number yield error message and leaves stack alone
    ((' x10+', deque([3])), ('ERROR: bad input', deque([3]))),

    # '+' return 'ERROR: insuffient stack' and leaves stack alone
    # if stack does not have enough on it
    (('+', deque([1])), ('ERROR: insufficient stack', deque([1]))),

    # '-' return 'ERROR: insuffient stack' and leaves stack alone
    # if stack does not have enough on it
    (('-', deque([1])), ('ERROR: insufficient stack', deque([1]))),

    (('&', deque([5, 3])), (None, deque([1]))),
    (('|', deque([5, 3])), (None, deque([7]))),
    (('^', deque([5, 3])), (None, deque([6]))),
    (('~', deque([3])), (None, deque([252]))),
    (('~', deque([])), ('ERROR: insufficient stack', deque([]))),
    (('s', deque([5, 3])), (None, deque([3, 5]))),
    (('s', deque([3])), ('ERROR: insufficient stack', deque([3]))),
    (('p', deque([5, 3])), (None, deque([5]))),
    (('p', deque([])), ('ERROR: insufficient stack', deque([]))),
    (('>>', deque([43, 2])), (None, deque([10]))),
    (('<<', deque([43, 2])), (None, deque([172]))),
)
@pytest.mark.parametrize('inputs, expected', cases)
def test_parse_line(inputs, expected):
    actual = parse_line(*inputs)
    assert expected == actual
