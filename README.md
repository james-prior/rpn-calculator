RPN Calculator
==============

This project has a crude RPN calculator for some 8-bit operations.

Use
---

Execute as:

    rpn.py

Requires Python 3.7.

It accepts one command per line.
After each command, the stack is printed with one line per value.
The bottom of the stack is printed first and
the top of the stack is shown last.

Each number on the stack is shown three ways:

1. 8 bit binary value
2. unsigned decimal (ala C signed char)
3. signed decimal (ala C unsigned char)

One may enter commands on the command line instead of into standard input,
but the input will not be output, so the output will be hard to follow.

### Number Input

Entering a number pushes it onto the stack (which is shown last).
Numbers are entered as a radix code followed by the number itself.
The radix codes are `b` for binary, `o` for octal,
`d` for decimal, and `x` for hexadecimal.
One may enter decimal numbers without the `d` prefix.
Numbers of all radixes may be negative.

#### Examples of valid number input

    b11111111
    b-10000
    o377
    o-20
    d255
    255
    xff
    xFF
    x-10
    -16

### Operations

#### Binary

    +
    -
    &
    |
    ^
    >>
    <<

#### Unary

    ~

#### Miscellaneous

    c   clear the stack
    p   pop (delete) one number off of top of stack (which is last number shown)
    s   swap top two items on stack (which are shown last)
    (empty line) show stack

### Example

The following example subtracts 3 from 4.
In this example, each command is one character long.
First `3` is entered,
then `4` is entered,
then the `-` command is entered.
The -1 result is shown as an 8-bit binary value,
as an unsigned decimal value,
and as a signed decimal value.

    $ rpn.py
    3
    00000011   3    3
    4
    00000011   3    3
    00000100   4    4
    -
    11111111 255   -1
    $

Testing
-------

To install stuff to do testing with,
execute the following command in the same directory as this documentation.
Requires Python 3.7 and `pipenv`.

    pipenv --python=3.7 install --dev pytest pytest-xdist

To run tests, execute the following in the same directory as this documentation.

    pipenv run pytest --color=yes -f .

To Do
-----

test-drive main (rewrite)
